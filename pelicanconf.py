#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

STATIC_PATHS = ['images']
AUTHOR = 'farooqkhan'
SITENAME = 'FarooqKhan Blog'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Asia/Kolkata'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Official Website','http://vsfarooqkhan.xyz'),
         ('GitLab Personal Website','https://vsfarooqkhan.gitlab.io/farooqkhan'),
         ('Github', 'https://github.com/vsfarooqkhan'),)

# Social widget
SOCIAL = (('LinkedIn', 'https://linkedin.com/in/vsfarooqkhan'),
          ('Dev.to', 'https://dev.to'),
          ('twitter','https://twitter.com/vsfarooqkhan'),
          ('stackoverflow','https://stackoverflow.com/users/8240120/farooq'),
          ('Medium','https://medium.com/@vsfarooqkhan')
          )

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
